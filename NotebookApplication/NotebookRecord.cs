﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class NoteBookRecord
    {
        readonly int id;
        string surname;
        string name;
        string middleName;
        string country;
        string telephoneNumber;
        DateTime birthDay;
        string organisation;
        string position;
        string note;

        public NoteBookRecord(int id)
        {
            this.id = id;
        }

        public string Surname { get => surname; set => surname = value; }
        public string Name { get => name; set => name = value; }
        public string MiddleName { get => middleName; set => middleName = value; }
        public string TelephoneNumber { get => telephoneNumber; set => telephoneNumber = value; }
        public string Country { get => country; set => country = value; }
        public int Id { get => id; }
        public DateTime BirthDay { get => birthDay; set => birthDay = value; }
        public string Organisation { get => organisation; set => organisation = value; }
        public string Position { get => position; set => position = value; }
        public string Note { get => note; set => note = value; }

        public void ShowShort()
        {
            Console.WriteLine(String.Format("{0, 5} {1, 20} {2, 20} {3, 20}", this.id, this.surname, this.name, this.telephoneNumber));
        }

        public void ShowFull()
        {
            Console.WriteLine("Id: " + this.id);
            Console.WriteLine($"{this.surname} {this.name} {this.middleName}");
            Console.WriteLine("Country: " + this.country);
            Console.WriteLine("Telephone Number: " + this.telephoneNumber);

            if (this.birthDay == DateTime.MinValue)
                Console.WriteLine("Birthday: ");
            else
                Console.WriteLine("Birthday: " + this.birthDay.ToShortDateString());

            Console.WriteLine("Organisation: " + this.organisation);
            Console.WriteLine("Position: " + this.position);
            Console.WriteLine("Other: " + this.note);
        }
    }
}
