﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class NoteBook
    {
        private static List<NoteBookRecord> records = new List<NoteBookRecord>();
        private static string currentCommand;
        private static int lastId = 0;

        static void Main(string[] args)
        {
            ShowGreeting();

            do
            {
                Console.WriteLine();
                Console.Write(">");
                currentCommand = Console.ReadLine();
                string[] commandParts = currentCommand.Split(' ');
                int id;

                switch (commandParts[0].ToLower())
                {
                    case "update":
                        if (commandParts.Length < 2 || commandParts.Length > 4)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }
                                                
                        if (commandParts.Length == 2)
                        {
                            if (!int.TryParse(commandParts[1], out id))
                            {
                                Console.WriteLine("Id should be a number");
                                break;
                            }

                            UpdateRecord(id);
                            break;
                        }

                        if (!int.TryParse(commandParts[1], out id))
                        {
                            Console.WriteLine("Id should be a number");
                            break;
                        }

                        NoteBookRecord record = FindRecordById(id);

                        if (record == null)
                        {
                            Console.WriteLine($"Record with id {id} not found");
                            break;
                        }

                        if (commandParts.Length == 3)
                        {
                            switch (commandParts[2].ToLower())
                            {
                                case "--surname":
                                case "--name":
                                case "--country":
                                case "--telephone":
                                    Console.WriteLine("It is mandatory field");
                                    break;

                                case "--middlename":
                                    record.MiddleName = "";
                                    break;

                                case "--organisation":
                                    record.Organisation = "";
                                    break;

                                case "--position":
                                    record.Position = "";
                                    break;

                                case "--note":
                                    record.Note = "";
                                    break;

                                case "--birthday":
                                    record.BirthDay = new DateTime();
                                    break;

                                default:
                                    Console.WriteLine("Command not found");
                                    break;
                            }
                        }

                        if (commandParts.Length == 4)
                        {                          
                            switch (commandParts[2].ToLower())
                            {
                                case "--surname":
                                    record.Surname = commandParts[3];
                                    break;

                                case "--name":
                                    record.Name = commandParts[3];
                                    break;

                                case "--middlename":
                                    record.MiddleName = commandParts[3];
                                    break;

                                case "--country":
                                    record.Country = commandParts[3];
                                    break;

                                case "--organisation":
                                    record.Organisation = commandParts[3];
                                    break;

                                case "--position":
                                    record.Position = commandParts[3];
                                    break;

                                case "--note":
                                    record.Note = commandParts[3];
                                    break;

                                case "--birthday":
                                    DateTime birthday;
                                    if (DateTime.TryParse(commandParts[3], out birthday))
                                    {
                                        record.BirthDay = birthday;
                                        break;
                                    }

                                    Console.WriteLine("Invalid value");
                                    break;

                                case "--telephone":
                                    if (commandParts[3].All(Char.IsDigit) && commandParts[3].Length >= 5 && commandParts[3].Length <= 15)
                                    {
                                        record.TelephoneNumber = commandParts[3];
                                        break;
                                    }

                                    Console.WriteLine("Invalid value");
                                    break;

                                default:
                                    Console.WriteLine("Command not found");
                                    break;
                            }
                        }

                        break;

                    case "find":
                        if (commandParts.Length < 2 || commandParts.Length > 3)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        switch (commandParts[1].ToLower())
                        {
                            case "--id":
                                if (commandParts.Length == 2)
                                {
                                    Console.WriteLine("Wrong number of arguments");
                                    break;
                                }

                                if (!int.TryParse(commandParts[2], out id))
                                {
                                    Console.WriteLine("Id should be a number");
                                    break;
                                }

                                ShowRecordById(id);
                                break;

                            case "--surname":
                                if (commandParts.Length == 2)
                                {
                                    Console.WriteLine("Wrong number of arguments");
                                    break;
                                }

                                ShowRecordsByName(commandParts[2]);
                                break;

                            case "--name":
                                if (commandParts.Length == 2)
                                {
                                    Console.WriteLine("Wrong number of arguments");
                                    break;
                                }

                                ShowRecordsByName(null, commandParts[2]);
                                break;

                            case "--country":
                                if (commandParts.Length == 2)
                                {
                                    Console.WriteLine("Wrong number of arguments");
                                    break;
                                }

                                ShowRecordsByCountry(commandParts[2]);
                                break;

                            case "--telephone":
                                if (commandParts.Length == 2)
                                {
                                    Console.WriteLine("Wrong number of arguments");
                                    break;
                                }

                                ShowRecordsByTelephoneNumber(commandParts[2]);
                                break;

                            case "--organisation":
                                if (commandParts.Length == 3)
                                {
                                    ShowRecordsByOrganisation(commandParts[2]);
                                }
                                else
                                {
                                    ShowRecordsByOrganisation("");
                                }
                                break;

                            default:
                                Console.WriteLine("Command not found");
                                break;
                        }

                        break;

                    case "create":
                        if (commandParts.Length != 1)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        CreateRecord();
                        break;

                    case "remove":
                        if (commandParts.Length != 2)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        if (!int.TryParse(commandParts[1], out id))
                        {
                            Console.WriteLine("Id should be a number");
                            break;
                        }

                        RemoveRecord(id);
                        break;

                    case "all":
                        if (commandParts.Length != 1)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        ShowAllRecords();
                        break;

                    case "help":
                        if (commandParts.Length != 1)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        ShowListOfCommands();
                        break;

                    case "quit":
                        if (commandParts.Length != 1)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        Environment.Exit(1);
                        break;

                    case "clear":
                        if (commandParts.Length != 1)
                        {
                            Console.WriteLine("Wrong number of arguments");
                            break;
                        }

                        Console.Clear();
                        break;

                    case "":
                        break;

                    default:
                        Console.WriteLine("Command not found");
                        break;
                }

            } while (true);
        }

        static void ShowGreeting()
        {
            Console.WriteLine("Hello! This is your Notebook application. Please, use the following commands to interact:");
            ShowListOfCommands();
        }

        static void ShowListOfCommands()
        {
            Console.WriteLine();
            Console.WriteLine("create - to create a new record in the notebook");
            Console.WriteLine("update [id] - to update information in the record with id = [id] interactively");
            Console.WriteLine("update [id] --surname [surname] - to update surname in the record with id = [id]");
            Console.WriteLine("update [id] --name [name] - to update name in the record with id = [id]");
            Console.WriteLine("update [id] --middlename [middlename] - to update middlename in the record with id = [id]");
            Console.WriteLine("update [id] --telephone [telephone] - to update telephone number in the record with id = [id]");
            Console.WriteLine("update [id] --birthday [birthday] - to update birthday in the record with id = [id]");
            Console.WriteLine("update [id] --country [country] - to update country in the record with id = [id]");
            Console.WriteLine("update [id] --organisation [organisation] - to update organisation in the record with id = [id]");
            Console.WriteLine("update [id] --position [position] - to update position in the record with id = [id]");
            Console.WriteLine("update [id] --note [note] - to update note in the record with id = [id]");
            Console.WriteLine("remove [id] - to remove the record with id = [id]");
            Console.WriteLine("all - to show all records in the notebook");
            Console.WriteLine("find --id [id] - to find the record with id = [id]");
            Console.WriteLine("find --surname [surname] - to find records with surname = [surname]");
            Console.WriteLine("find --name [name] - to find records with name = [name]");
            Console.WriteLine("find --telephone [telephone] - to find records with telephone number = [telephone]");
            Console.WriteLine("find --country [country] - to find records with country = [country]");
            Console.WriteLine("find --organisation [organisation] - to find records with organisation = [organisation]");
            Console.WriteLine("help - to get a list of commands");
            Console.WriteLine("clear - to clear the console");
            Console.WriteLine("quit - to close the application");
        }

        static void ShowAllRecords()
        {
            if (records.Count == 0)
            {
                Console.WriteLine("No records found");
            }
            else
            {
                foreach (NoteBookRecord record in records)
                {
                    record.ShowShort();
                    Console.WriteLine();
                }
            }
        }

        static void ShowRecordById(int id)
        {
            NoteBookRecord record = FindRecordById(id);

            if (record == null)
            {
                Console.WriteLine($"Record with id {id} not found");
            }
            else
            {
                record.ShowFull();
            }
        }

        static void ShowRecordsByName(string surname = null, string name = null)
        {
            List<NoteBookRecord> foundRecords = FindRecordsBySurnameAndName(surname, name);

            if (foundRecords.Count == 0)
            {
                if (surname != null && name != null)
                {
                    Console.WriteLine($"No records with surname {surname} and name {name} found");
                }
                else
                {
                    if (surname != null)
                    {
                        Console.WriteLine($"No records with surname {surname} found");
                    }
                    if (name != null)
                    {
                        Console.WriteLine($"No records with name {name} found");
                    }
                }

                return;
            }

            foreach (var record in foundRecords)
            {
                record.ShowFull();
                Console.WriteLine();
            }
        }

        static void ShowRecordsByTelephoneNumber(string telephoneNumber)
        {
            List<NoteBookRecord> foundRecords = FindRecordsByTelephoneNumber(telephoneNumber);

            if (foundRecords.Count == 0)
            {
                Console.WriteLine($"No records with telephone number {telephoneNumber} found");
            }
            else
            {
                foreach (var record in foundRecords)
                {
                    record.ShowFull();
                    Console.WriteLine();
                }
            }
        }

        static void ShowRecordsByCountry(string country)
        {
            List<NoteBookRecord> foundRecords = FindRecordsByCountry(country);

            if (foundRecords.Count == 0)
            {
                Console.WriteLine($"No records with country {country} found");
            }
            else
            {
                foreach (var record in foundRecords)
                {
                    record.ShowFull();
                    Console.WriteLine();
                }
            }
        }

        static void ShowRecordsByOrganisation(string organisation)
        {
            List<NoteBookRecord> foundRecords = FindRecordsByOrganisation(organisation);

            if (foundRecords.Count == 0)
            {
                Console.WriteLine($"No records with organisation {organisation} found");
            }
            else
            {
                foreach (var record in foundRecords)
                {
                    record.ShowFull();
                    Console.WriteLine();
                }
            }
        }

        static void CreateRecord()
        {
            NoteBookRecord newRecord = new NoteBookRecord(++lastId);
            Console.WriteLine("Please, enter following information to create a new record. If you want to skip a field, press Enter");

            while (true)
            {
                Console.Write("Surname (mandatory): ");
                string surname = Console.ReadLine();

                if (surname != "")
                {
                    newRecord.Surname = surname;
                    break;
                }

                Console.WriteLine("Surname is the mandatory field!");
            }

            while (true)
            {
                Console.Write("Name (mandatory): ");
                string name = Console.ReadLine();

                if (name != "")
                {
                    newRecord.Name = name;
                    break;
                }

                Console.WriteLine("Name is the mandatory field!");
            }

            Console.Write("Middle name: ");
            newRecord.MiddleName = Console.ReadLine();

            while (true)
            {
                Console.Write("Telephone number (only digits, mandatory): ");
                string telephoneNumber = Console.ReadLine();

                if (telephoneNumber.All(Char.IsDigit) && telephoneNumber.Length >= 5 && telephoneNumber.Length <= 15)
                {
                    newRecord.TelephoneNumber = telephoneNumber;
                    break;
                }

                Console.WriteLine("Invalid value");
            }

            while (true)
            {
                Console.Write("Country (mandatory): ");
                string country = Console.ReadLine();

                if (country != "")
                {
                    newRecord.Country = country;
                    break;
                }

                Console.WriteLine("Country is the mandatory field!");
            }

            while (true)
            {
                Console.Write("Birthday: ");
                DateTime birthday;
                string birthdayString = Console.ReadLine();

                if (birthdayString == "")
                {
                    break;
                }

                if (DateTime.TryParse(birthdayString, out birthday))
                {
                    newRecord.BirthDay = birthday;
                    break;
                }

                Console.WriteLine("Invalid value");
            }

            Console.Write("Organisation: ");
            newRecord.Organisation = Console.ReadLine();

            Console.Write("Position: ");
            newRecord.Position = Console.ReadLine();

            Console.Write("Other notes: ");
            newRecord.Note = Console.ReadLine();

            records.Add(newRecord);
        }

        static bool AskIfNeedToChange()
        {
            while (true)
            {
                Console.WriteLine("Change? Press y or n");
                string answer = Console.ReadLine();
                if (answer.ToLower() == "y")
                {
                    return true;
                }
                else if (answer.ToLower() == "n")
                {
                    return false;
                }
                else
                {
                    Console.WriteLine("Invalid command");
                }
            }
        }

        static void UpdateRecord(int id)
        {
            NoteBookRecord record = FindRecordById(id);

            if (record == null)
            {
                Console.WriteLine($"Record with id {id} not found");
                return;
            }

            Console.WriteLine("Surname: " + record.Surname);
            if (AskIfNeedToChange())
            {
                while (true)
                {
                    Console.Write("Surname (mandatory): ");
                    string surname = Console.ReadLine();

                    if (surname != "")
                    {
                        record.Surname = surname;
                        break;
                    }

                    Console.WriteLine("Surname is the mandatory field!");
                }
            }

            Console.WriteLine("Name: " + record.Name);
            if (AskIfNeedToChange())
            {
                while (true)
                {
                    Console.Write("Name (mandatory): ");
                    string name = Console.ReadLine();

                    if (name != "")
                    {
                        record.Name = name;
                        break;
                    }

                    Console.WriteLine("Name is the mandatory field!");
                }
            }

            Console.WriteLine("Middle name: " + record.MiddleName);
            if (AskIfNeedToChange())
            {
                Console.Write("Middle name: ");
                record.MiddleName = Console.ReadLine();
            }

            Console.WriteLine("Telephone number: " + record.TelephoneNumber);
            if (AskIfNeedToChange())
            {
                while (true)
                {
                    Console.Write("Telephone number (only digits, mandatory): ");
                    string telephoneNumber = Console.ReadLine();

                    if (telephoneNumber.All(Char.IsDigit) && telephoneNumber.Length >= 5 && telephoneNumber.Length <= 15)
                    {
                        record.TelephoneNumber = telephoneNumber;
                        break;
                    }

                    Console.WriteLine("Invalid value");
                }
            }

            Console.WriteLine("Country: " + record.Country);
            if (AskIfNeedToChange())
            {
                while (true)
                {
                    Console.Write("Country (mandatory): ");
                    string country = Console.ReadLine();

                    if (country != "")
                    {
                        record.Country = country;
                        break;
                    }

                    Console.WriteLine("Country is the mandatory field!");
                }
            }

            if (record.BirthDay == DateTime.MinValue)
                Console.WriteLine("Birthday: ");
            else
                Console.WriteLine("Birthday: " + record.BirthDay.ToShortDateString());

            if (AskIfNeedToChange())
            {
                while (true)
                {
                    Console.Write("Birthday: ");
                    DateTime birthday;

                    if (DateTime.TryParse(Console.ReadLine(), out birthday))
                    {
                        record.BirthDay = birthday;
                        break;
                    }

                    Console.WriteLine("Invalid value");
                }
            }

            Console.WriteLine("Organisation: " + record.Organisation);
            if (AskIfNeedToChange())
            {
                Console.Write("Organisation: ");
                record.Organisation = Console.ReadLine();
            }

            Console.WriteLine("Position: " + record.Position);
            if (AskIfNeedToChange())
            {
                Console.Write("Position: ");
                record.Position = Console.ReadLine();
            }

            Console.WriteLine("Other notes: " + record.Note);
            if (AskIfNeedToChange())
            {
                Console.Write("Other notes: ");
                record.Note = Console.ReadLine();
            }
        }

        static void RemoveRecord(int id)
        {
            NoteBookRecord record = FindRecordById(id);

            if (record == null)
            {
                Console.WriteLine($"Record with id {id} not found");
                return;
            }

            records.Remove(record);
        }

        static NoteBookRecord FindRecordById(int id)
        {
            foreach (NoteBookRecord record in records)
            {
                if (record.Id == id)
                {
                    return record;
                }
            }

            return null;
        }

        static List<NoteBookRecord> FindRecordsBySurnameAndName(string surname = null, string name = null)
        {
            List<NoteBookRecord> foundRecords = new List<NoteBookRecord>();

            if (name != null && surname != null)
            {
                foreach (NoteBookRecord record in records)
                {
                    if (record.Surname == surname && record.Name == name)
                    {
                        foundRecords.Add(record);
                    }
                }
            }
            else if (name == null)
            {
                foreach (NoteBookRecord record in records)
                {
                    if (record.Surname == surname)
                    {
                        foundRecords.Add(record);
                    }
                }
            }
            else
            {
                foreach (NoteBookRecord record in records)
                {
                    if (record.Name == name)
                    {
                        foundRecords.Add(record);
                    }
                }
            }

            return foundRecords;
        }

        static List<NoteBookRecord> FindRecordsByTelephoneNumber(string telephoneNumber)
        {
            List<NoteBookRecord> foundRecords = new List<NoteBookRecord>();

            foreach (NoteBookRecord record in records)
            {
                if (record.TelephoneNumber == telephoneNumber)
                {
                    foundRecords.Add(record);
                }
            }

            return foundRecords;
        }

        static List<NoteBookRecord> FindRecordsByCountry(string country)
        {
            List<NoteBookRecord> foundRecords = new List<NoteBookRecord>();

            foreach (NoteBookRecord record in records)
            {
                if (record.Country == country)
                {
                    foundRecords.Add(record);
                }
            }

            return foundRecords;
        }

        static List<NoteBookRecord> FindRecordsByOrganisation(string organisation)
        {
            List<NoteBookRecord> foundRecords = new List<NoteBookRecord>();

            foreach (NoteBookRecord record in records)
            {
                if (record.Organisation == organisation)
                {
                    foundRecords.Add(record);
                }
            }

            return foundRecords;
        }
    }
}
